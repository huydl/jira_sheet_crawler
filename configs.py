import os

JIRA_URL = os.environ.get("JIRA_CWL_SERVER_URL", "https://jira.vmo.dev/")

OUTPUT_DIR = "output"
