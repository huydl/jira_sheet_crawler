import sys
from typing import Dict, List, Optional

from jira import JIRA, resources
from loguru import logger
from PyQt5.QtWidgets import QApplication

from src.frontend import (
    CrawlTypeForm,
    DateForm,
    ExportForm,
    FromToForm,
    LoginForm,
    ProjectForm,
)


def login_call():
    app_login = QApplication(sys.argv)
    login_form = LoginForm()
    login_form.show()
    app_login.exec_()

    return login_form.auth_jira


def homepage_call():
    app_homepage = QApplication(sys.argv)
    homepage = CrawlTypeForm()
    homepage.show()
    app_homepage.exec_()

    return homepage.flag


def crawl_all_form_call(auth_jira: JIRA, projects: Optional[List[resources.Project]] = None):
    from src.crawler import get_all_project_worklogs

    logworks = get_all_project_worklogs(auth_jira=auth_jira, projects=projects)
    logger.info(f"Found {len(logworks)} logworks")

    return logworks


def crawl_date_form_call(auth_jira: JIRA, projects: Optional[List[resources.Project]] = None):
    app_form = QApplication(sys.argv)
    date_form = DateForm(auth_jira=auth_jira, projects=projects)
    date_form.show()
    app_form.exec_()

    return date_form.logworks


def crawl_from_to_form_call(auth_jira: JIRA, projects: Optional[List[resources.Project]] = None):
    app_form = QApplication(sys.argv)
    date_form = FromToForm(auth_jira=auth_jira, projects=projects)
    date_form.show()
    app_form.exec_()

    return date_form.logworks


def project_form_call(auth_jira: JIRA):
    app_form = QApplication(sys.argv)
    project_form = ProjectForm(auth_jira)
    project_form.show()
    app_form.exec_()

    return project_form.projects


def export_form_call(lising_logworks: Dict[str, List[float]]):
    app_form = QApplication(sys.argv)
    export_form = ExportForm(lising_logworks)
    export_form.show()
    app_form.exec_()


def main():
    auth_jira = login_call()
    try:
        while True:
            projects = project_form_call(auth_jira=auth_jira)

            crawl_type = homepage_call()

            if crawl_type == 0:
                listing_logworks = crawl_all_form_call(auth_jira=auth_jira, projects=projects)
            elif crawl_type == 1:
                listing_logworks = crawl_date_form_call(auth_jira=auth_jira, projects=projects)
            elif crawl_type == 2:
                listing_logworks = crawl_from_to_form_call(auth_jira=auth_jira, projects=projects)

            export_form_call(listing_logworks)

    except Exception as e:
        logger.error(e)
        sys.exit()


if __name__ == "__main__":
    main()
