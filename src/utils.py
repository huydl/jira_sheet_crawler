from typing import Dict, List, Union

import pandas as pd
from jira import JIRA
from loguru import logger

from configs import JIRA_URL


def compress_time_date(worklogs: List[Dict[str, Union[str, float]]]) -> Dict[str, int]:
    new_worklogs = {}
    for log in worklogs:
        encrypt = "{}#{}#{}#{}/{}/{}".format(
            log["project"],
            log["author"].displayName,
            log["author"].emailAddress,
            log["started"].day,
            log["started"].month,
            log["started"].year,
        )
        new_worklogs[encrypt] = new_worklogs.get(encrypt, 0) + log["spent"]

    return new_worklogs


def mapping_worklogs(compress_worklogs: Dict[str, List[float]]) -> Dict[str, List[float]]:
    listing_worklogs = {}
    for encrypt, spent in compress_worklogs.items():
        project, name, email, date = encrypt.split("#")
        day, month, year = date.split("/")
        key = project + "#" + name + "#" + email + "#" + month + "/" + year

        if key not in listing_worklogs:
            listing_worklogs[key] = [0.0 for i in range(31)]
        listing_worklogs[key][int(day) - 1] = spent

    return listing_worklogs


def connect_jira(jira_user: str, jira_password: str, jira_server: str = JIRA_URL) -> JIRA:
    """
    Connect to JIRA. Return None on error
    """
    try:
        logger.info("Connecting to JIRA: %s" % jira_server)
        jira_options = {"server": jira_server}
        jira = JIRA(options=jira_options, basic_auth=(jira_user, jira_password))
        return jira
    except Exception as e:
        logger.error("Failed to connect to JIRA: %s" % e)
        return None


def post_process_worklogs(
    worklogs: List[Dict[str, Union[str, float]]], isSorted: bool = True
) -> Dict[str, List[float]]:
    compress_worklogs = compress_time_date(worklogs)
    listing_worklogs = mapping_worklogs(compress_worklogs)
    # sort by date
    if isSorted:
        listing_worklogs = {
            k: v for k, v in sorted(listing_worklogs.items(), key=lambda x: x[0].split("#")[-1])
        }

    return listing_worklogs


def group_worklogs_by_date(worklogs: Dict[str, List[float]]) -> Dict[str, Dict[str, List[float]]]:
    new_dict = {}
    for k, v in worklogs.items():
        _, _, _, date = k.split("#")
        if date not in new_dict:
            new_dict[date] = {}
        new_dict[date][k] = v

    return new_dict


def dict_2_df(worklogs: Dict[str, List[float]]) -> pd.DataFrame:
    columns = ["Project", "User", "Email", "Logged"] + list(range(1, 32))
    df_dict = {k: [] for k in columns}

    for k, v in worklogs.items():
        project, name, email, _ = k.split("#")

        df_dict["Project"].append(project)
        df_dict["User"].append(name)
        df_dict["Email"].append(email)
        df_dict["Logged"].append(sum(v))

        for i in range(len(v)):
            df_dict[i + 1].append(v[i])

    return pd.DataFrame.from_dict(df_dict, orient="columns")


def aggregate_by_user(worklogs: List[Dict[str, Union[str, float]]]) -> Dict[str, float]:
    aggregation = {}
    for log in worklogs:
        encrypt = "{}#{}".format(log["author"].displayName, log["author"].emailAddress)
        aggregation[encrypt] = aggregation.get(encrypt, 0) + log["spent"]

    return aggregation


def aggregation_to_df(aggregation: Dict[str, Union[str, float]]) -> pd.DataFrame:
    aggregation_dict = {"Name": [], "Email": [], "Total Logged": []}
    for key, value in aggregation.items():
        name, email = key.split("#")
        aggregation_dict["Name"].append(name)
        aggregation_dict["Email"].append(email)
        aggregation_dict["Total Logged"].append(value)

    aggregation_df = pd.DataFrame.from_dict(aggregation_dict)
    return aggregation_df
