from typing import Dict, List

import pygsheets
import xlsxwriter
from loguru import logger

from src.utils import aggregation_to_df, dict_2_df, group_worklogs_by_date


def default_header_row(worksheet: xlsxwriter.worksheet.Worksheet):
    row, column = 0, 0
    HEADER = ["Project", "User", "Email", "Logged"]
    for col in HEADER:
        worksheet.write(row, column, col)
        column += 1

    for i in range(31):
        worksheet.write(row, column, i + 1)
        column += 1


def export_xlsx(file_name: str, listing_worklogs: Dict[str, List[float]]):
    # Initialize xlsx file
    workbook = xlsxwriter.Workbook(file_name)

    # Fill spent time by day
    for key, spent_by_date in listing_worklogs.items():
        project, name, email, date = key.split("#")
        date = date.replace("/", "_")
        if date not in workbook.sheetnames:
            row = 0
            worksheet = workbook.add_worksheet(date)
            # Initialize header row
            default_header_row(worksheet)

        row += 1

        worksheet.write(row, 0, project)
        worksheet.write(row, 1, name)
        worksheet.write(row, 2, email)
        worksheet.write(row, 3, sum(spent_by_date))
        for i in range(4, len(spent_by_date) + 4):
            worksheet.write(row, i, spent_by_date[i - 4])

    workbook.close()


def export_gsheet(credentials_path: str, file_name: str, logworks: Dict[str, List[float]]):
    gc = pygsheets.authorize(service_file=credentials_path)
    try:
        spreadsheet = gc.open(file_name)
    except Exception as e:
        logger.error(e)
        return

    logworks = group_worklogs_by_date(logworks)

    for key_date, dict_value in logworks.items():
        logger.info("Write date", key_date)
        dataframe = dict_2_df(dict_value)

        try:
            wks = spreadsheet.worksheet_by_title(key_date)
        except Exception:
            wks = spreadsheet.add_worksheet(key_date)

        wks.set_dataframe(dataframe, (0, 0))


def export_aggregation_local(file_name: str, aggregation: Dict[str, float]):
    import pandas as pd

    aggregation_df = aggregation_to_df(aggregation)

    with pd.ExcelWriter(file_name, mode="a", engine="openpyxl") as writer:
        aggregation_df.to_excel(writer, sheet_name="Aggregation", index=False)


def export_aggregation_gsheet(credentials_path: str, file_name: str, aggregation: Dict[str, float]):
    gc = pygsheets.authorize(service_file=credentials_path)
    try:
        spreadsheet = gc.open(file_name)
    except Exception as e:
        logger.error(e)
        return

    aggregation_df = aggregation_to_df(aggregation)

    try:
        wks = spreadsheet.worksheet_by_title("Aggregation")
    except Exception:
        wks = spreadsheet.add_worksheet("Aggregation")

    wks.set_dataframe(aggregation_df, (0, 0))
