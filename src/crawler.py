import calendar
import datetime
from collections import defaultdict
from typing import Dict, List, Optional, Union

from jira import JIRA, resources
from loguru import logger


def get_worklog_by_date(
    auth_jira: JIRA,
    project: resources.Project,
    month: Optional[Union[int, List[int]]] = None,
    year: Optional[Union[int, List[int]]] = None,
    round_spent_time: int = 2,
):
    logger.info("Get spent issue worklog from project key {}".format(project.key))

    jql = "timespent > 0 AND project = {} ORDER BY updated DESC".format(project.key)
    issues = auth_jira.search_issues(jql)

    if month is not None and year is not None:
        if isinstance(month, int) and isinstance(year, int):
            num_days = calendar.monthrange(year, month)[1]
            from_date = datetime.date(year, month, 1)
            to_date = datetime.date(year, month, num_days)
        else:
            num_days = calendar.monthrange(year[1], month[1])[1]
            from_date = datetime.date(year[0], month[0], 1)
            to_date = datetime.date(year[1], month[1], num_days)

    worklogs = []
    date_worklogs = defaultdict(list)
    issue_worklogs = defaultdict(list)
    issues_data = {}

    for issue in issues:
        issues_data[issue.key] = issue
        for w in auth_jira.worklogs(issue.key):
            started = datetime.datetime.strptime(w.started[:-5], "%Y-%m-%dT%H:%M:%S.%f")

            if (
                month is not None
                and year is not None
                and not (from_date <= started.date() <= to_date)
            ):
                continue

            spent = round(w.timeSpentSeconds / 3600, round_spent_time)

            logger.info(
                "Log date {} spent {} hour(s) on {} from member {}".format(
                    started.date(), spent, issue.key, w.author
                )
            )
            worklog = {
                "started": started,
                "spent": spent,
                "author": w.author,
                "issue": issue,
                "project": project.name + " - " + project.key,
            }
            worklogs.append(worklog)
            date_worklogs[started.date()].append(worklog)
            issue_worklogs[issue.key].append(worklog)

    return worklogs, date_worklogs, issue_worklogs, issues_data


def get_all_project_worklogs(
    auth_jira: JIRA, projects: Optional[List[resources.Project]] = None
) -> List[Dict[str, Union[str, float]]]:
    total_worklogs = []
    if projects is None:
        projects = auth_jira.projects()
    for project in projects:
        worklogs, _, _, _ = get_worklog_by_date(auth_jira, project)
        total_worklogs.extend(worklogs)
    return total_worklogs


def get_all_project_worklogs_by_date(
    auth_jira: JIRA, month: int, year: int, projects: Optional[List[resources.Project]] = None
) -> List[Dict[str, Union[str, float]]]:
    total_worklogs = []
    if projects is None:
        projects = auth_jira.projects()
    for project in projects:
        worklogs, _, _, _ = get_worklog_by_date(auth_jira, project, month, year)
        total_worklogs.extend(worklogs)
    return total_worklogs


def get_all_project_worklogs_from_to_date(
    auth_jira: JIRA,
    from_month: int,
    from_year: int,
    to_month: int,
    to_year: int,
    projects: Optional[List[resources.Project]] = None,
) -> List[Dict[str, Union[str, float]]]:
    total_worklogs = []
    if projects is None:
        projects = auth_jira.projects()
    for project in projects:
        worklogs, _, _, _ = get_worklog_by_date(
            auth_jira,
            project,
            month=[from_month, to_month],
            year=[from_year, to_year],
        )

        total_worklogs.extend(worklogs)

    return total_worklogs
