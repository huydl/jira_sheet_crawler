from PyQt5.QtWidgets import (
    QGridLayout,
    QLabel,
    QLineEdit,
    QMessageBox,
    QPushButton,
    QWidget,
)

from src.utils import connect_jira


class LoginForm(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Login Form")
        self.resize(500, 120)

        self.layout = QGridLayout()

        self._username()
        self._password()
        self._login()

        self.setLayout(self.layout)

    def _username(self):
        # Username
        label_name = QLabel('<font size="4"> Username </font>')
        self.lineEdit_username = QLineEdit()
        self.lineEdit_username.setPlaceholderText("Please enter your username")

        self.layout.addWidget(label_name, 0, 0)
        self.layout.addWidget(self.lineEdit_username, 0, 1)

    def _password(self):
        # Password
        label_password = QLabel('<font size="4"> Password </font>')
        self.lineEdit_password = QLineEdit()
        self.lineEdit_password.setEchoMode(QLineEdit.Password)
        self.lineEdit_password.setPlaceholderText("Please enter your password")

        self.layout.addWidget(label_password, 1, 0)
        self.layout.addWidget(self.lineEdit_password, 1, 1)

    def _login(self):
        # Login button
        button_login = QPushButton("Login")
        button_login.clicked.connect(self.check_password)
        self.layout.addWidget(button_login, 2, 0, 1, 2)
        self.layout.setRowMinimumHeight(2, 75)

    def check_password(self):
        msg = QMessageBox()

        self.auth_jira = connect_jira(
            jira_user=self.lineEdit_username.text(),
            jira_password=self.lineEdit_password.text(),
        )

        if self.auth_jira is not None:
            self.close()
        else:
            msg.setText("Incorrect username or password")
            msg.exec_()
