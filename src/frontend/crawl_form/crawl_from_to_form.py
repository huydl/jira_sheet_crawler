from datetime import datetime
from typing import List, Optional

from jira import JIRA, resources
from loguru import logger
from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import (
    QDateEdit,
    QGridLayout,
    QLabel,
    QMessageBox,
    QPushButton,
    QWidget,
)

from src.crawler import get_all_project_worklogs_from_to_date


class FromToForm(QWidget):
    def __init__(self, auth_jira: JIRA, projects: Optional[List[resources.Project]] = None):
        super().__init__()
        self.auth_jira = auth_jira
        self.projects = projects

        self.setWindowTitle("Crawl from date to date Form")
        self.resize(500, 120)

        self.layout = QGridLayout()

        self._fromDate()
        self._toDate()
        self._crawl_button()

        self.setLayout(self.layout)

    def _fromDate(self):
        label_name = QLabel("Choose specific FROM month and year")
        self.layout.addWidget(label_name, 0, 0)

        self.fromDateEdit = QDateEdit()
        self.fromDateEdit.setCurrentSection(QDateEdit.Section.MonthSection)
        self.fromDateEdit.setDisplayFormat("MM/yyyy")
        self.fromDateEdit.setDate(QDate.currentDate())

        self.fromDateEdit.setCalendarPopup(True)

        self.layout.addWidget(self.fromDateEdit, 0, 1)

    def _toDate(self):
        label_name = QLabel("Choose specific TO month and year")
        self.layout.addWidget(label_name, 1, 0)

        self.toDateEdit = QDateEdit()
        self.toDateEdit.setCurrentSection(QDateEdit.Section.MonthSection)
        self.toDateEdit.setDisplayFormat("MM/yyyy")
        self.toDateEdit.setDate(QDate.currentDate())

        self.toDateEdit.setCalendarPopup(True)
        self.layout.addWidget(self.toDateEdit, 1, 1)

    def _crawl_button(self):
        self.crawl_button = QPushButton("Crawl")
        self.crawl_button.clicked.connect(self._crawl)
        self.layout.addWidget(self.crawl_button, 3, 0, 1, 0)

    def _crawl(self):
        self.from_month, self.from_year = (
            self.fromDateEdit.date().month(),
            self.fromDateEdit.date().year(),
        )
        self.to_month, self.to_year = self.toDateEdit.date().month(), self.toDateEdit.date().year()
        if datetime(day=1, month=self.from_month, year=self.from_year) > datetime(
            day=1, month=self.to_month, year=self.to_year
        ):
            msg = QMessageBox()
            msg.setText("FROM date must be less or equal than TO date")
            msg.exec_()
        else:
            self.logworks = get_all_project_worklogs_from_to_date(
                self.auth_jira,
                from_month=self.from_month,
                from_year=self.from_year,
                to_month=self.to_month,
                to_year=self.to_year,
                projects=self.projects,
            )
            logger.info(f"Found {len(self.logworks)} logworks")

            self.close()
