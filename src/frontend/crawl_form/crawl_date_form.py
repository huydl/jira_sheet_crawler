from typing import List, Optional

from jira import JIRA, resources
from loguru import logger
from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import QDateEdit, QGridLayout, QLabel, QPushButton, QWidget

from src.crawler import get_all_project_worklogs_by_date


class DateForm(QWidget):
    def __init__(self, auth_jira: JIRA, projects: Optional[List[resources.Project]] = None):
        super().__init__()
        self.auth_jira = auth_jira
        self.projects = projects

        self.setWindowTitle("Crawl by date Form")
        self.resize(500, 120)

        self.layout = QGridLayout()

        self._form()
        self._crawl_button()

        self.setLayout(self.layout)

    def _form(self):
        label_name = QLabel("Choose specific month and year")
        self.layout.addWidget(label_name, 0, 0)

        self.dateEdit = QDateEdit()
        self.dateEdit.setCurrentSection(QDateEdit.Section.MonthSection)
        self.dateEdit.setDisplayFormat("MM/yyyy")
        self.dateEdit.setDate(QDate.currentDate())

        self.dateEdit.setCalendarPopup(True)
        self.layout.addWidget(self.dateEdit, 0, 1)

    def _crawl_button(self):
        self.crawl_button = QPushButton("Crawl")
        self.crawl_button.clicked.connect(self._crawl)
        self.layout.addWidget(self.crawl_button, 1, 0, 1, 0)

    def _crawl(self):
        month, year = self.dateEdit.date().month(), self.dateEdit.date().year()
        self.logworks = get_all_project_worklogs_by_date(
            auth_jira=self.auth_jira, month=month, year=year, projects=self.projects
        )
        logger.info(f"Found {len(self.logworks)} logworks")

        self.close()
