from PyQt5.QtWidgets import QGridLayout, QLabel, QMessageBox, QPushButton, QWidget


class CrawlTypeForm(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Home Page")
        self.resize(500, 120)

        self.layout = QGridLayout()
        self._title()
        self._crawl_all_time()
        self._crawl_single_date()
        self._crawl_from_to_date()

        self.msg = QMessageBox()

        self.setLayout(self.layout)

    def _title(self):
        title = QLabel('<font size="5"> CRAWL WORKLOGS FROM ALL PROJECTS </font>')
        self.layout.addWidget(title, 0, 0, 1, 0)

    def _crawl_all_time(self):
        all_time_button = QPushButton("All time")
        all_time_button.clicked.connect(self._crawl_all_time_form)
        self.layout.addWidget(all_time_button, 1, 0)
        self.layout.setRowMinimumHeight(2, 100)

    def _crawl_single_date(self):
        single_date_button = QPushButton("Single date")
        single_date_button.clicked.connect(self._crawl_single_date_form)
        self.layout.addWidget(single_date_button, 2, 0)
        self.layout.setRowMinimumHeight(2, 50)

    def _crawl_from_to_date(self):
        crawl_all_button = QPushButton("From date to date")
        crawl_all_button.clicked.connect(self._crawl_from_to_date_form)
        self.layout.addWidget(crawl_all_button, 3, 0)
        self.layout.setRowMinimumHeight(2, 50)

    def _crawl_all_time_form(self):
        self.flag = 0
        self.close()

    def _crawl_single_date_form(self):
        self.flag = 1
        self.close()

    def _crawl_from_to_date_form(self):
        self.flag = 2
        self.close()
