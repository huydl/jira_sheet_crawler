from jira import JIRA
from loguru import logger
from PyQt5.QtWidgets import (
    QCheckBox,
    QGridLayout,
    QLabel,
    QMessageBox,
    QPushButton,
    QWidget,
)


class ProjectForm(QWidget):
    def __init__(self, auth_jira: JIRA):
        super().__init__()
        self.all_projects = auth_jira.projects()

        self.setWindowTitle("Projects")
        self.resize(500, 120)
        self.layout = QGridLayout()

        self._title()

        self._form()
        self._button()

        self.setLayout(self.layout)

    def _title(self):
        title = QLabel('<font size="5"> CHOOSE PROJECT </font>')
        self.layout.addWidget(title, 0, 0, 1, 0)

    def _form(self):
        self.checkboxes = [QCheckBox(project.name) for project in self.all_projects]
        for i, checkbox in enumerate(self.checkboxes):
            if i & 1:
                self.layout.addWidget(checkbox, (i + 2) // 2, 0)
            else:
                # self.layout.addWidget(checkbox, i - len(self.checkboxes) // 2 + 2, 1)
                self.layout.addWidget(checkbox, (i + 2) // 2, 1)

    def _button(self):
        single_date_button = QPushButton("Select all")
        single_date_button.clicked.connect(self.button_select_all)
        self.layout.addWidget(single_date_button, len(self.all_projects) + 1, 0)

        single_date_button = QPushButton("Unselect all")
        single_date_button.clicked.connect(self.button_unselect_all)
        self.layout.addWidget(single_date_button, len(self.all_projects) + 1, 1)

        single_date_button = QPushButton("Next")
        single_date_button.clicked.connect(self.button_next_step)
        self.layout.addWidget(single_date_button, len(self.all_projects) + 2, 0, 1, 0)

    def button_select_all(self):
        for checkbox in self.checkboxes:
            checkbox.setChecked(True)

    def button_unselect_all(self):
        for checkbox in self.checkboxes:
            checkbox.setChecked(False)

    def button_next_step(self):
        self.projects = [
            self.all_projects[i]
            for i in range(len(self.all_projects))
            if self.checkboxes[i].isChecked()
        ]
        if len(self.projects) > 0:
            logger.info(f"Selected {len(self.projects)} projects")
            for i in range(len(self.projects)):
                logger.info(f"{self.projects[i].name}")
            self.close()
        else:
            msg = QMessageBox()
            msg.setText("Please choose at least one project")
            msg.exec_()
