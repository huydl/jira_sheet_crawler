import os
from typing import Dict, List

from loguru import logger
from PyQt5.QtWidgets import (
    QFileDialog,
    QGridLayout,
    QLabel,
    QLineEdit,
    QMessageBox,
    QPushButton,
    QWidget,
)

from configs import OUTPUT_DIR
from src.export import (
    export_aggregation_gsheet,
    export_aggregation_local,
    export_gsheet,
    export_xlsx,
)
from src.utils import aggregate_by_user, post_process_worklogs


class ExportForm(QWidget):
    def __init__(self, logworks: Dict[str, List[float]]):
        super().__init__()
        self.listing_logworks = post_process_worklogs(logworks)
        self.aggregate_logworks = aggregate_by_user(logworks)

        self.setWindowTitle("Export Form")
        self.resize(500, 120)

        self.layout = QGridLayout()

        self._form()
        self._export_local_button()
        self._export_gsheet_button()

        os.makedirs(OUTPUT_DIR, exist_ok=True)
        self.credentials = None

        self.setLayout(self.layout)

    def _form(self):
        label = QLabel('<font size="4"> File name </font>')
        self.layout.addWidget(label, 0, 0)

        self.lineEdit_filename = QLineEdit()
        self.layout.addWidget(self.lineEdit_filename, 0, 1)

        self.credentials_button = QPushButton("Open file")
        self.credentials_button.clicked.connect(self._get_credentials)
        self.layout.addWidget(self.credentials_button, 1, 0)

        self.credentials_path = QLabel("No credentials found")
        self.layout.addWidget(self.credentials_path, 1, 1)

    def _get_credentials(self):
        self.credentials = QFileDialog.getOpenFileName(self, "Open file", "", "JSON Files (*.json)")
        if self.credentials:
            self.credentials_path.setText(self.credentials[0])
        else:
            self.credentials_path.setText("No credentials found")

    def _export_local_button(self):
        button_login = QPushButton("Export LOCAL")
        button_login.clicked.connect(self._export_local)
        self.layout.addWidget(button_login, 2, 0, 1, 0)

    def _export_gsheet_button(self):
        button_login = QPushButton("Export GSHEET")
        button_login.clicked.connect(self._export_gsheet)
        self.layout.addWidget(button_login, 3, 0, 1, 0)

    def _export_local(self):
        if len(self.lineEdit_filename.text()) != 0:
            export_xlsx(
                os.path.join(OUTPUT_DIR, self.lineEdit_filename.text() + ".xlsx"),
                self.listing_logworks,
            )
            export_aggregation_local(
                os.path.join(OUTPUT_DIR, self.lineEdit_filename.text() + ".xlsx"),
                self.aggregate_logworks,
            )
            logger.info("Export DONE")
            self.close()
        else:
            msg = QMessageBox()
            msg.setText("Please ensure filename")
            msg.exec_()

    def _export_gsheet(self):
        if len(self.lineEdit_filename.text()) != 0 and self.credentials:
            export_gsheet(self.credentials[0], self.lineEdit_filename.text(), self.listing_logworks)
            export_aggregation_gsheet(
                self.credentials[0], self.lineEdit_filename.text(), self.aggregate_logworks
            )
            logger.info("Export DONE")
            self.close()
        else:
            msg = QMessageBox()
            msg.setText("Please ensure filename and credentials")
            msg.exec_()
