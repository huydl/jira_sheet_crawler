import argparse
import os

from loguru import logger

from src.crawler import (
    get_all_project_worklogs,
    get_all_project_worklogs_by_date,
    get_all_project_worklogs_from_to_date,
)
from src.export import export_xlsx
from src.utils import connect_jira, post_process_worklogs


def cli():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--mode",
        required=True,
        type=str,
        choices=["all", "range", "month"],
        default="all",
    )

    parser.add_argument("--from-month", type=int, help="Start month")
    parser.add_argument("--from-year", type=int, help="Start year")

    parser.add_argument("--to-month", type=int, help="End month")
    parser.add_argument("--to-year", type=int, help="End year")

    parser.add_argument(
        "--output",
        type=str,
        help="Output directory to store xlsx",
        required=True,
        default="results",
    )

    args = parser.parse_args()
    return args.__dict__


def main(args):
    auth_jira = connect_jira()

    if args.get("mode") == "all":
        logworks = get_all_project_worklogs(auth_jira)
    elif args.get("mode") == "month":
        if not args.get("from_month") and not args.get("from_year"):
            logger.error("Please specify from_month and from_year")
            return

        logworks = get_all_project_worklogs_by_date(
            auth_jira, month=args.get("from_month"), year=args.get("from_year")
        )
    elif args.get("mode") == "range":
        if (
            not args.get("from_month")
            and not args.get("from_year")
            and not args.get("to_month")
            and not args.get("to_year")
        ):
            logger.error("Please specify from_month, from_year, to_month and to_year")
            return

        logworks = get_all_project_worklogs_from_to_date(
            auth_jira,
            from_month=args.get("from_month"),
            from_year=args.get("from_year"),
            to_month=args.get("to_month"),
            to_year=args.get("to_year"),
        )
    else:
        logger.error(f"Unknown mode {args.get('mode')}")
        return

    logger.info(f"Found {len(logworks)} logworks")
    listing_worklogs = post_process_worklogs(logworks)

    os.makedirs(args.get("output"), exist_ok=True)
    if args.get("mode") == "all":
        export_xlsx(
            os.path.join(args.get("output"), "reports.xlsx"),
            listing_worklogs,
        )
    elif args.get("mode") == "month":
        export_xlsx(
            os.path.join(
                args.get("output"),
                "report_{}_{}.xlsx".format(args.get("from_month"), args.get("from_year")),
            ),
            listing_worklogs,
        )
    else:
        export_xlsx(
            os.path.join(
                args.get("output"),
                "report_{}_{}_to_{}_{}.xlsx".format(
                    args.get("from_month"),
                    args.get("from_year"),
                    args.get("to_month"),
                    args.get("to_year"),
                ),
            ),
            listing_worklogs,
        )
    logger.info("Exporting logworks to xlsx finished")


if __name__ == "__main__":
    args = cli()
    main(args)
