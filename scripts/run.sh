#!/bin/bash
export PYTHONPATH=.

# Export all projects
python main.py --mode all

# Export all projects in a single month
python main.py \
    --mode month \
    --from-month 4 \
    --from-year 2023

# Export all projects in a range time
python main.py \
    --mode range \
    --from-month 1 \
    --from-year 2023 \
    --to-month 4 \
    --to-year 2023