@echo off
pip install virtualenv==20.21.0
python -m virtualenv py_env

py_env\Scripts\python -m pip install --upgrade pip
py_env\Scripts\python -m pip install -r requirements.txt