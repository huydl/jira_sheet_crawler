# Jira Sheet Crawler

## Introduction
- A simple crawler tool for taking all logworks at Jira.
- Making xlsx report format
- Environment: `python3.9`

## Linux usage
1. Install all the dependencies
```
pip install -m requirements.txt
```

2. Command usage:
- All the details:
```
export PYTHONPATH=.

python main.py [-h] -m {all,range,month} [--from-month FROM_MONTH] [--from-year FROM_YEAR] [--to-month TO_MONTH] [--to-year TO_YEAR] --output OUTPUT

optional arguments:
  -h, --help            show this help message and exit
  -m {all,range,month}, --mode {all,range,month}
  --from-month FROM_MONTH
                        Start month
  --from-year FROM_YEAR
                        Start year
  --to-month TO_MONTH   End month
  --to-year TO_YEAR     End year
  --output OUTPUT       Output directory to store xlsx
```
- *Optional:* Sample bash script at `scripts/run.sh`

3. Application usage:
- Command:
```
export PYTHONPATH=.

python app.py 
```
- (Optional): Execute bash script: `scripts/app.sh`

## Windows compatibility
The easiest way to use this application in Windows is to run `setup_wins.bat` and `run_wins.bat` in order.

## Conclusion

If you have any questions, suggestions or issues, please contact us by [email](mailto:huydl@vmodev.com).

### Happy coding :)